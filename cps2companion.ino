// CONFIGURATION

// available pinout values:
//     1001: CPS2 sound eprom (1-2)
//     4096: CPS2 code eprom (3-10)
//     322:  CPS2 qsound / gfx eprom (11-20)
#define PINOUT 1001

// For 322 pinouts, you can force WORD_MODE on the A20 pin
// Necessary for SO jumpers configurations (with chips like 23C1610, 23C8100C or 27C160)
//#define FORCE_WORD_MODE 1

// Select the CN where you inject the security key
// CN2 for boards B-3 and B-4. CN9 for others.
#define INJECTION_CN 9


// ------

// CPS2 Injection pins
#if INJECTION_CN == 9
    // CN9
    #define DATA        44  //CN9 #2
    #define SETUP1      46  //CN9 #3
    #define CLOCK       48  //CN9 #4
    #define SETUP2      50  //CN9 #5
#else
    // CN2
    #define DATA        44  //CN2 A32
    #define CLOCK       46  //CN2 A31
    #define SETUP1      48  //CN2 A30
    #define SETUP2      50  //CN2 A29
#endif

#define CPS2_CLOCK_LENGTH 25

// ROM settings
#define IMPULSE_LENGTH 50
#define ENABLE_VPP 41

#if PINOUT == 4096
    // 27c4096
    #define ROM_SIZE 512*1024L
    #define TOTAL_ADDRESS_PINS 18
    #define TOTAL_DATA_PINS 16

    byte address_pins[] = {
      18, 17, 16, 15, 14, 43, 42, 2, 3,
      5, 6, 7, 8, 9, 10, 11, 12, 13
    };

    byte data_pins[] = {
      37, 36, 35, 34, 33, 32, 31, 30, 28, 27, 26, 25, 24, 23, 22, 21
    };

    #define CE_PIN 20
    #define OE_PIN 38
    #define P_PIN CE_PIN
    #define GND_PIN 29
    #define GND_PIN_2 4
#endif

#if PINOUT == 322
    // 27c322
    #define TOTAL_DATA_PINS 16

    byte address_pins[] = {
            29, 28, 27, 26, 25,
            24, 23, 22, 12, 11,
            10, 9, 8, 7, 6,
            5, 4, 21, 20, 13,
            3
    };

    byte data_pins[] = {
            33, 35, 37, 39,
            19, 17, 15, 43,
            34, 36, 38, 40,
            18, 16, 14, 42
    };

    #define CE_PIN 30
    #define OE_PIN ENABLE_VPP
    #define P_PIN CE_PIN
    // force A20 to VCC like CPS2 does with its jumpers
    #ifdef FORCE_WORD_MODE
        #define TOTAL_ADDRESS_PINS 20
        #define ROM_SIZE 2048*1024L
        #define WORD_MODE_PIN 3
    #else
        #define TOTAL_ADDRESS_PINS 21
        #define ROM_SIZE 4096*1024L
    #endif

    #define GND_PIN 31
    #define GND_PIN_2 2
#endif

#if PINOUT == 1001
    // 27c1001
    #define ROM_SIZE 128*1024L
    #define TOTAL_ADDRESS_PINS 17
    #define TOTAL_DATA_PINS 8

    byte address_pins[] = {
      30, 29, 28, 27,
      26, 25, 24, 23,
      9, 8, 5, 7,
      22, 10, 11, 21,
      20
    };

    byte data_pins[] = {
      31, 32, 33, 14,
      43, 42, 2, 3
    };

    #define CE_PIN 4
    #define OE_PIN 6
    #define P_PIN 13
    #define GND_PIN 34
    #define GND_PIN_2 GND_PIN
#endif

#define TOTAL_ADDRESSES (1L << TOTAL_ADDRESS_PINS)
// END ROM DEFINITION


void setupCPS2() {
    pinMode(SETUP1, OUTPUT);
    pinMode(SETUP2, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(DATA, OUTPUT);

    digitalWrite(SETUP1, LOW);
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);
    digitalWrite(SETUP2, HIGH);
}

void setup() {
    pinMode(OE_PIN, OUTPUT);
    pinMode(CE_PIN, OUTPUT);
    pinMode(P_PIN, OUTPUT);
    pinMode(ENABLE_VPP, OUTPUT);
    pinMode(GND_PIN, OUTPUT);
    pinMode(GND_PIN_2, OUTPUT);
    pinMode(2, OUTPUT);
    for (int i = 0; i < TOTAL_ADDRESS_PINS; i++) {
        pinMode(address_pins[i], OUTPUT);
    }
    for (int i = 0; i < TOTAL_DATA_PINS; i++) {
        pinMode(data_pins[i], INPUT);
    }
    Serial.begin(250000);
    delay(1000);

    digitalWrite(CE_PIN, HIGH);
    digitalWrite(OE_PIN, HIGH);
    digitalWrite(P_PIN, HIGH);
    digitalWrite(ENABLE_VPP, HIGH);
    digitalWrite(GND_PIN, LOW);
    digitalWrite(GND_PIN_2, LOW);
#ifdef WORD_MODE_PIN
    digitalWrite(WORD_MODE_PIN, HIGH);
#endif
    // setup cps2 injection
    setupCPS2();
}

void inputMode() {
    for (int i = 0; i < TOTAL_DATA_PINS; i++) {
        pinMode(data_pins[i], INPUT);
    }
}

void outputMode() {
    for (int i = 0; i < TOTAL_DATA_PINS; i++) {
        pinMode(data_pins[i], OUTPUT);
        digitalWrite(data_pins[i], LOW);
    }
}

void setAddress(uint32_t value) {
    uint32_t address = 1;
    for (int i = 0; i < TOTAL_ADDRESS_PINS; i++) {
        digitalWrite(address_pins[i], (value & address) > 0);
        address = address << 1;
    }
}

uint16_t readBytes() {
    byte current = 0;
    uint16_t data = 0;
    delayMicroseconds(1);
    for (int i = TOTAL_DATA_PINS - 1; i >= 0; i--) {
        data = data << 1;
        current = digitalRead(data_pins[i]);
        data |= current & 1;
    }
    return data;
}

void programBytes(uint16_t data) {
    uint16_t address = 1;
    for (int i = 0; i < TOTAL_DATA_PINS; i++) {
        digitalWrite(data_pins[i], (data & address) > 0);
        address = address << 1;
    }
    //P pulse
    delayMicroseconds(4);
    digitalWrite(P_PIN, LOW);
    delayMicroseconds(IMPULSE_LENGTH);
    digitalWrite(P_PIN, HIGH);
    delayMicroseconds(4);
}

bool writeROM() {
    unsigned long address = 0;
    uint16_t data = 0;
    //start ce/oe at HIGH, impulse will be sent via /P (may be /CE)
#if CE_PIN == P_PIN
    digitalWrite(CE_PIN, HIGH);
#else
    digitalWrite(CE_PIN, LOW);
#endif
    digitalWrite(OE_PIN, HIGH);
    outputMode();
    setAddress(0);
    delayMicroseconds(100);
    for (long i = 0; i < TOTAL_ADDRESSES; i++) {
        setAddress(address++);
        delayMicroseconds(4);
#if TOTAL_DATA_PINS == 16
        while (Serial.available() < 2);
        data = Serial.read();
        data |= Serial.read() << 8;
#else
        while (Serial.available() == 0);
        data = Serial.read();
#endif
        programBytes(data);
        if (i == 0) {
            // first bytes, generate a second impulse to be sure
            programBytes(data);
        }
        if ((i % 16) == 0) {
            // sync point
            Serial.write(1);
        }
    }
#if CE_PIN != P_PIN
    digitalWrite(CE_PIN, HIGH);
#endif
}

int readROM() {
    unsigned long address = 0;
    uint16_t data = 0;
    //read mode
    inputMode();
    digitalWrite(CE_PIN, LOW);
    digitalWrite(OE_PIN, LOW);
    for (long i = 0; i < TOTAL_ADDRESSES; i++) {
        setAddress(address++);
        // impulse to make sure that the address is taken into account (27c4096 needs that)
        digitalWrite(OE_PIN, HIGH);
        digitalWrite(OE_PIN, LOW);
        data = readBytes();
        Serial.write(data & 255);
#if TOTAL_DATA_PINS == 16
        Serial.write(data >> 8);
#endif
    }
    setAddress(0);
    digitalWrite(OE_PIN, HIGH);
    digitalWrite(CE_PIN, HIGH);
}

void programCPS2() {
    setupCPS2();
    uint8_t to_write;
    // setup the cps2 in write mode
    digitalWrite(SETUP1, HIGH);
    digitalWrite(SETUP2, LOW);
    delay(CPS2_CLOCK_LENGTH);
    delay(CPS2_CLOCK_LENGTH);
    delay(CPS2_CLOCK_LENGTH);
    delay(CPS2_CLOCK_LENGTH);
    for (int i = 0; i < 20; i++) {
        // get the byte to write
        while (Serial.available() < 1);
        to_write = Serial.read();

        Serial.print(to_write, HEX);
        Serial.print(" ");
        for (int b = 7; b >= 0; b--) {
            digitalWrite(DATA, (to_write & (1 << b)) != 0 ? HIGH : LOW);

            // clock pulse
            digitalWrite(CLOCK, HIGH);
            delay(CPS2_CLOCK_LENGTH);
            digitalWrite(CLOCK, LOW);
            delay(CPS2_CLOCK_LENGTH);
        }
    }
    digitalWrite(SETUP1, LOW);
    digitalWrite(SETUP2, HIGH);
    delay(CPS2_CLOCK_LENGTH);
    digitalWrite(CLOCK, LOW);
    Serial.write('Z');
}

byte in_byte = 0;
void loop() {
    long adr;
    if (Serial.available()) {
        in_byte = Serial.read();
        if (in_byte == 0x55) {
            while (Serial.available() == 0);
            in_byte = Serial.read();
            switch (in_byte) {
                case 'w':
                    writeROM();
                    break;
                case 'r':
                    readROM();
                    break;
                case 'c':
                    programCPS2();
                    break;
                case 'S':
                    unsigned long romsize = ROM_SIZE;
                    unsigned long datapins = TOTAL_DATA_PINS;
                    Serial.write((byte*) &romsize, sizeof(romsize));
                    Serial.write((byte*) &datapins, sizeof(datapins));
                    break;

                    // debug commands
                case 'p':
                    adr = Serial.parseInt();
                    outputMode();
                    programBytes(adr);
                    inputMode();
                    Serial.print("programmed bytes ");
                    Serial.print(adr);
                    Serial.print("\n");
                    break;
                case 'O':
                    digitalWrite(OE_PIN, HIGH);
                    Serial.print("/OE is HIGH\n");
                    break;
                case 'o':
                    digitalWrite(OE_PIN, LOW);
                    Serial.print("/OE is LOW\n");
                    break;
                case 'E':
                    digitalWrite(CE_PIN, HIGH);
                    Serial.print("/CE is HIGH\n");
                    break;
                case 'e':
                    digitalWrite(CE_PIN, LOW);
                    Serial.print("/CE is LOW\n");
                    break;
                case 'n':
                    adr = Serial.parseInt();
                    setAddress(adr);
                    Serial.print("Address is");
                    Serial.print(adr);
                    Serial.print("\n");
                    break;
                case 'b':
                    adr = readBytes();
                    Serial.print("Read bytes are");
                    Serial.print(adr);
                    Serial.print("\n");
                    break;
            }
        }
    }
}
