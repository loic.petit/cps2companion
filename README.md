# WydD's CPS2 Companion

This project aims to help you fiddle with your CPS2: EPROM read, EPROM burn and security key injection.

This is a shield to be on top of an Arduino Mega. It could be used to play with other types of EPROM so feel free to
poke around. The main advantage is that the total cost of this device is pretty low compared to programmers that
actually supports up to 42 pins roms.

## Disclaimer
This is something I've worked on by assembling bits of knowledge here and there, and I thought it would be a good idea
to share this work. If you damage stuff with it, you're on your own.

## Hardware
This is how I built my rig, but you may get creative (design your PCB layout etc...) 
![Shield](shield.png)

I used the [following prototype board](https://www.amazon.fr/WINGONEER-Prototype-Arduino-Shield-Board/dp/B01FTWJR9I/) as
a base ground. There are others so whatever fits your needs. I won't share the layout because I don't think it's great.

### Power
As you may know, EPROM burning requires high voltage rails (12.5V and 6.25V usually). To have that I used a boost 
converter to convert the original 5V to 12.5V (called V12 is the schematics).
Then I apply a standard LM371 regulator to lower the voltage to 6.25V (V6 in the schematics). My current setup involves
a fixed 270Ω - 870Ω bridge but a potentiometer would be better.

If you aim to implement a new EPROM type, check that the voltage ranges are good for your power lines.

To bind the voltage rails to the actual Vpp/Vcc on the ROM chips, I'm using the following schematic. The DPDT switch 
will allow to switch between the read mode and the programming mode.

![Enable Vpp Schematics](enable_vpp.png)
 
### Pinout
On the shield, I have put a 44 pin DIP socket (a ZIF would be better but they are more rare to find). Here is how you
should bind each pin. 

![pinout](pinout.png)

You can see that each chip has a specific place to accomodate to the placement of Vpp/Vcc.

Notice that I added a little switch on pin 14. We need to be able to switch between Vpp or Arduino 32. This switch is
useful only in the case of the 27c322 pinout.

I also added 0.1µF capacitors near the Vcc pins.

Finally, for the CPS2 key injection, I added a 4 DIP socket connected to pins 44-46-48-50 of the Arduino.

## Software
You need python 3.x with the following package installed (using pip install for instance): `tqdm`, `pyserial` and `readline`.

### Chip selection & Upload
Open the project in the Arduino IDE, change the PINOUT definition on top of the file (follow the instructions there), 
save and upload to your device.

### Run
To execute the program, just launch the following command and follow the instructions.
`python cps2companion.py`

## Operations
### Read
*MAKE SURE YOU ARE IN READ MODE*

This will dump the entire content of the EPROM in a file. The maximum speed is about 9000 bytes/s.

### Burn
*MAKE SURE YOU ARE IN PROGRAMMING MODE*

This will burn the content of a file in an EPROM. If the file is smaller that the capacity of the EPROM, its content is 
duplicated (e.g. a 1MB file in a 4MB EPROM).

The maximum speed is about 2000 writes/s.

### Blank check
*MAKE SURE YOU ARE IN READ MODE*

This will check that all bytes are equal to 0xFF.

### Verify
*MAKE SURE YOU ARE IN READ MODE*

This will check that an EPROM corresponds to a file. This is a shortcut to dumping the content and do a check with 
something like `vbindiff` afterwards.

### Security Key Injection
Before injection, make sure that you know what you're dealing with by reading the [master blog post on this topic](http://arcadehacker.blogspot.com/2016/09/capcom-cps2-security-programming-guide.html).

Then select your CN in the `cps2companion.ino` file. The pinouts are the following:
```c
// CN9
#define DATA        44  //CN9 #2
#define SETUP1      46  //CN9 #3
#define CLOCK       48  //CN9 #4
#define SETUP2      50  //CN9 #5
```

```c
// CN2
#define DATA        44  //CN2 A32
#define CLOCK       46  //CN2 A31
#define SETUP1      48  //CN2 A30
#define SETUP2      50  //CN2 A29
```

Follow the exact instruction on the original blog post, just replace the part where you select using the on board screen
by the operation here.

## Thanks
This work was heavily inspired by the [Robson Couto's eprom tool](https://github.com/robsoncouto/eprom) check out his
blog post on writing a 27c801 for the SNES. Even thought the final product is completely different now, the first
prototype was his work with small adjustments.

The CPS2 security injection is clearly a transformed version of the [ArcadeHacker CPS2 Programming](https://github.com/ArcadeHacker/ArcadeHacker_CPS2) tool.
This is an immense amount of reverse engineering packed in a handy tool in the end.
